const express = require('express');
const app = express();
const port = ${{values.port}};

app.get('/', (req, res) => {
    res.send('${{values.application}} - A Node.js app deployed by backstage');
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
