# Aviatrix Smart Group Resource

## Description

The `aviatrix_smart_group` resource handles the creation and management of Smart Groups within the Aviatrix platform.
